to run the game as it was meant to be played please follow the following steps:

1.open the game
2.right click on the top bar and select Defaults
3.In the Options window, check "use legacy console"
4.In the Font window, choose Raster Fonts font, and 8x8 as size
5.In the Layout window, choose Width at least 87 and Height at least 66, both Screen Buffer Size and Window Size
6.Relaunch the game