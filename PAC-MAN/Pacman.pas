Program pacman;
uses crt,dos,sysutils;
type matrix=array[1..22,1..17] of byte;
     smat=array[1..20] of byte;
     per=record
              n:string[3];
              s:word;
          end;
     arpers=array[1..255] of per;
var k:char;
    i:byte;
    xm,ym:byte;
Function now:longword;
var h,m,s,ms:word;
Begin
    gettime(h,m,s,ms);
    now:=h*360000+m*6000+s*100+ms;
end;
Procedure empty(x,y:byte;VAR v:matrix);
Begin
     case v[x,y] of 0:Begin
                           textcolor(black);
                           gotoxy((x-1)*4,(y-1)*4);
                           write('����');
                           gotoxy((x-1)*4,(y-1)*4+1);
                           write('����');
                           gotoxy((x-1)*4,(y-1)*4+2);
                           write('����');
                           gotoxy((x-1)*4,(y-1)*4+3);
                           write('����');
                        end;
                      2:Begin
                           textcolor(cyan);
                           gotoxy((x-1)*4,(y-1)*4);
                           write('    ');
                           gotoxy((x-1)*4,(y-1)*4+1);
                           write(' �� ');
                           gotoxy((x-1)*4,(y-1)*4+2);
                           write(' �� ');
                           gotoxy((x-1)*4,(y-1)*4+3);
                           write('    ');
                        end;
                   end;
end;
Procedure ghost(x,y,c:byte;s:boolean);
Begin
     textcolor(c);
     gotoxy((x-1)*4,(y-1)*4);
     write('����');
     gotoxy((x-1)*4,(y-1)*4+1);
     textcolor(black);
     textbackground(c);
     write(' �� ');
     textcolor(c);
     textbackground(black);
     gotoxy((x-1)*4,(y-1)*4+2);
     write('����');
     gotoxy((x-1)*4,(y-1)*4+3);
     if s then
        write('����')
     else
        write('����');
end;
Procedure pacman(x,y,dir,c:byte);
var xs,ys:byte;
Begin
     textcolor(c);
     case dir of 1:Begin
                      gotoxy((x-1)*4,(y-1)*4);
                      write('��');
                      gotoxy((x-1)*4,(y-1)*4+1);
                      write('��');
                      gotoxy((x-1)*4+3,(y-1)*4+1);
                      write('�');
                      gotoxy((x-1)*4,(y-1)*4+2);
                      write('� ��');
                      gotoxy((x-1)*4,(y-1)*4+3);
                      write('����');
                   end;
                 2:Begin
                      gotoxy((x-1)*4,(y-1)*4);
                      write('����');
                      gotoxy((x-1)*4,(y-1)*4+1);
                      write('�� �');
                      gotoxy((x-1)*4+2,(y-1)*4+2);
                      write('��');
                      gotoxy((x-1)*4+1,(y-1)*4+3);
                      write('���');
                   end;
                 3:Begin
                      gotoxy((x-1)*4,(y-1)*4);
                      write('����');
                      gotoxy((x-1)*4,(y-1)*4+1);
                      write('� ��');
                      gotoxy((x-1)*4,(y-1)*4+2);
                      write('��');
                      gotoxy((x-1)*4,(y-1)*4+3);
                      write('���');
                   end;
                 4:Begin
                      gotoxy((x-1)*4,(y-1)*4);
                      write('���� ');
                      gotoxy((x-1)*4,(y-1)*4+1);
                      write('�� �');
                      gotoxy((x-1)*4,(y-1)*4+2);
                      write('�');
                      gotoxy((x-1)*4+2,(y-1)*4+2);
                      write('��');
                      gotoxy((x-1)*4+2,(y-1)*4+3);
                      write('��');
                   end;
              end;
end;
Function AI(x,y,d:byte;VAR v:matrix):byte;
var o:array[1..4] of byte;
    n:byte;
Begin
    n:=0;
    if (v[x,y-1]<>1) and (d<>4) then
       Begin
          inc(n);
          o[n]:=1;
       end;
    if (v[x-1,y]<>1) and (d<>3) then
       Begin
          inc(n);
          o[n]:=2;
       end;
    if (v[x+1,y]<>1) and (d<>2) then
       Begin
          inc(n);
          o[n]:=3;
       end;
    if (v[x,y+1]<>1) and (d<>1) then
       Begin
          inc(n);
          o[n]:=4;
       end;
    AI:=o[random(n)+1];
end;
Function gameover(x,y:byte;VAR xg,yg:smat;nrghosts:byte):boolean;
var i:byte;
    sem:boolean;
Begin
     sem:=false;
     for i:=1 to nrghosts do
        sem:=((sem) or ((xg[i]=x) and (yg[i]=y)));
     gameover:=sem;
end;
Procedure Game;
var v:matrix;
    f:text;
    i,j,pm,px,dir,r,x,y,xo,yo,level,nrghosts,speed:byte;
    xg,yg,xog,yog,dg:smat;
    k:char;
    score,coins:word;
    time1,time2:longword;
    s,lvlup:boolean;
    lvl:string[12];
    nume:string[3];
Begin
     level:=1;
     score:=0;
     assign(f,'options.in');
     reset(f);
     readln(f,nrghosts,speed);
     close(f);
     repeat
         coins:=0;
         lvlup:=false;
         str(level,lvl);
         lvl:='LEVELS\'+lvl+'.lvl';
         assign(f,lvl);
         reset(f);
         clrscr;
         readln(f,x,y);
         for i:=1 to nrghosts do
             Begin
                 xg[i]:=x;
                 yg[i]:=y;
             end;
         readln(f,x,y);
         j:=0;
         while not eof(f) do
            Begin
                 inc(j);
                 i:=0;
                 while not eoln(f) do
                    Begin
                         inc(i);
                         read(f,v[i,j]);
                    end;
                 readln(f);
            end;
         close(f);
         for i:=1 to 17 do
            for j:=1 to 22 do
                    case v[j,i] of 1:Begin
                                         textcolor(lightgray);
                                         if (v[j,i-1]=1) then
                                            Begin
                                              gotoxy((j-1)*4+1,(i-1)*4);
                                              write('��');
                                            end;
                                         if (v[j,i+1]=1) and (i<17) then
                                            Begin
                                              gotoxy((j-1)*4+1,(i-1)*4+3);
                                              write('��');
                                            end;
                                         if (v[j-1,i]=1) then
                                            Begin
                                              gotoxy((j-1)*4,(i-1)*4+1);
                                              write('�');
                                              gotoxy((j-1)*4,(i-1)*4+2);
                                              write('�');
                                            end;
                                         if (v[j+1,i]=1) and (j<22) then
                                            Begin
                                              gotoxy((j-1)*4+3,(i-1)*4+1);
                                              write('�');
                                              gotoxy((j-1)*4+3,(i-1)*4+2);
                                              write('�');
                                            end;
                                         gotoxy((j-1)*4+1,(i-1)*4+1);
                                         write('��');
                                         gotoxy((j-1)*4+1,(i-1)*4+2);
                                         write('��');
                                     end;
                                   2:Begin
                                       textcolor(cyan);
                                       gotoxy((j-1)*4+1,(i-1)*4+1);
                                       write('��');
                                       gotoxy((j-1)*4+1,(i-1)*4+2);
                                       write('��');
                                       inc(coins);
                                    end;
                            end;
         dir:=2;k:=chr(0);
         time2:=now;
         repeat
              if keypressed then
                    k:=readkey;
              if k=#13 then
                 Begin
                   gotoxy(33,28);
                   write('�������� PAUSE �����ͻ');
                   gotoxy(33,29);
                   write('�  SCORE:',score:7,'     �');
                   gotoxy(33,30);
                   write('�  LEVEL:',level:7,'     �');
                   gotoxy(33,31);
                   write('��������������������ͼ');
                   repeat
                     k:=readkey;
                   until (k=#13) or (k=#27);
                   for j:=9 to 14 do
                      case v[j,8] of 0:Begin
                                         gotoxy((j-1)*4,28);
                                         write('    ');
                                         gotoxy((j-1)*4,29);
                                         write('    ');
                                         gotoxy((j-1)*4,30);
                                         write('    ');
                                         gotoxy((j-1)*4,31);
                                         write('    ');
                                       end;
                                     1:Begin
                                         textcolor(lightgray);
                                         gotoxy((j-1)*4,28);
                                         write('    ');
                                         gotoxy((j-1)*4,29);
                                         write(' �� ');
                                         gotoxy((j-1)*4,30);
                                         write(' �� ');
                                         gotoxy((j-1)*4,31);
                                         write('    ');
                                         if (v[j,8-1]=1) then
                                           Begin
                                             gotoxy((j-1)*4+1,28);
                                             write('��');
                                           end;
                                         if (v[j,i+1]=1) then
                                           Begin
                                             gotoxy((j-1)*4+1,31);
                                             write('��');
                                           end;
                                         if (v[j-1,8]=1) then
                                           Begin
                                             gotoxy((j-1)*4,29);
                                             write('�');
                                             gotoxy((j-1)*4,30);
                                             write('�');
                                           end;
                                         if (v[j+1,8]=1) then
                                           Begin
                                             gotoxy((j-1)*4+3,29);
                                             write('�');
                                             gotoxy((j-1)*4+3,30);
                                             write('�');
                                           end;
                                       end;
                                     2:Begin
                                         textcolor(cyan);
                                         gotoxy((j-1)*4+1,29);
                                         write('��');
                                         gotoxy((j-1)*4+1,30);
                                         write('��');
                                       end;
                                  end;
                     end;
              time1:=now;
              if time1>=time2+speed then
                 Begin
                     time2:=time1;
                     case k of#72:dir:=1;
                              #80:dir:=4;
                              #75:dir:=2;
                              #77:dir:=3;
                         end;
                     if k<>#27 then k:=chr(0);
                     xo:=x;
                     yo:=y;
                     case dir of 1:if v[x,y-1]<>1 then dec(y);
                                 2:if v[x-1,y]<>1 then dec(x);
                                 3:if v[x+1,y]<>1 then inc(x);
                                 4:if v[x,y+1]<>1 then inc(y);
                         end;
                     if gameover(x,y,xg,yg,nrghosts) then k:=#27;
                     if v[x,y]=2 then
                        Begin
                           score:=score+10;
                           v[x,y]:=0;
                           dec(coins);
                        end;
                     xog:=xg;
                     yog:=yg;
                     for i:=1 to nrghosts do
                        Begin
                           dg[i]:=AI(xg[i],yg[i],dg[i],v);
                           case dg[i] of 1:dec(yg[i]);
                                         2:dec(xg[i]);
                                         3:inc(xg[i]);
                                         4:inc(yg[i]);
                                      end;
                           empty(xog[i],yog[i],v);
                        end;
                     empty(xo,yo,v);
                     if k<>#27 then
    		        for i:=1 to nrghosts do
    	                    ghost(xg[i],yg[i],magenta,s)
                     else
                        for i:=1 to nrghosts do
                           ghost(xog[i],yog[i],magenta,s);
                     if gameover(x,y,xg,yg,nrghosts) then k:=#27;
                     if k=#27 then
    		        pacman(x,y,dir,brown)
                     else
                        pacman(x,y,dir,yellow);
                     s:=not s;
                     if coins=0 then lvlup:=true;
                 end;
        until (k=#27) or lvlup;
        if coins=0 then inc(level);
    until (k=#27) or (level=5);
    delay(500);
    gotoxy(33,28);
    if coins=0 then
         write('������ GAME  WON ���ͻ')
    else
         write('������ GAME OVER ���ͻ');
    gotoxy(33,29);
    write('�  SCORE:',score:7,'     �');
    gotoxy(33,30);
    write('�  LEVEL:',level:7,'     �');
    gotoxy(33,31);
    write('��������������������ͼ');
    gotoxy(35,32);
    write('�   FINAL SCORE  �');
    score:=score+level*15;
    gotoxy(35,33);
    write('�   ',score:7,'      �');
    gotoxy(35,34);
    write('�   NAME:        �');
    gotoxy(35,35);
    write('����������������ͼ');
    gotoxy(46,34);
    readln(nume);
    assign(f,'SCORES.IN');
    append(f);
    case length(nume) of 0:writeln(f,'    ',score);
                         1:writeln(f,upcase(nume[1]),'   ',score);
                         2:writeln(f,upcase(nume[1]),upcase(nume[2]),'  ',score);
                         3:writeln(f,upcase(nume[1]),upcase(nume[2]),upcase(nume[3]),' ',score);
                     end;
    close(f);
end;
Procedure Help;
var i:byte;
Begin
     clrscr;
     textcolor(white);
     writeln;
     Writeln('   SCOPUL JOCULUI:          ');
     textcolor(lightgray);
     writeln;
     writeln(' scopul jocului este de     ');
     writeln(' a aduna toate punctele     ');
     writeln(' si de a te ferii de        ');
     writeln(' fantome                    ');
     writeln;
     writeln('                            ');
     textcolor(white);
     writeln('    FANTOME:              ');
     textcolor(lightgray);
     writeln('                            ');
     writeln('         -Alege un drum     ');
     writeln('          aleatoriu.        ');
     writeln('                            ');
     writeln('                            ');
     writeln('                            ');
     writeln;
     writeln;
     writeln;
     writeln;
     writeln('         -Urmareste         ');
     writeln('          jucatorul daca    ');
     writeln('          acesta este         ');
     writeln('          aproape.         ');
     writeln('                            ');
     writeln;
     writeln;
     writeln('         -Parcurge un       ');
     writeln('          drum              ');
     writeln('          prestabilit       ');
     ghost(2,4,magenta,true);
     ghost(2,6,blue,true);
     ghost(2,8,red,true);
     textcolor(white);
     gotoxy(5,38);
     writeln('PUNCTE:');
     textcolor(cyan);
     gotoxy(5,41);write('��');
     gotoxy(5,42);write('��');
     textcolor(lightgray);
     gotoxy(10,40);write('-Pentru a ');
     gotoxy(11,41);write('putea trece');
     gotoxy(11,42);write('la nivelul');
     gotoxy(11,43);write('urmator');
     gotoxy(11,44);write('trebuie sa ');
     gotoxy(11,45);write('aduni toate');
     gotoxy(11,46);write('punctele.');
     for i:=1 to 55 do
         Begin
             gotoxy(27,i);
             write(#179);
         end;
     textcolor(white);
     gotoxy(34,2);write('SCOR:');
     textcolor(lightgray);
     gotoxy(29,5);write('Scorul este calculat');
     gotoxy(29,6);write('folosind urmatoarea');
     gotoxy(29,7);write('formula:');
     textcolor(white);
     gotoxy(32,10);write('S=P*10+L*15');
     textcolor(lightgray);
     gotoxy(29,13);write('Unde ',#96,'S',#39,' reprezinta');
     gotoxy(29,14);write('scorul final,',#96,'P',#39);
     gotoxy(29,15);write('reprezinta numarul');
     gotoxy(29,16);write('de puncte adunate,');
     gotoxy(29,17);write('iar ',#96,'L',#39,'reprezinta');
     gotoxy(29,18);write('nivelul maxim atins');
     gotoxy(29,19);write('pe durata jocului.');
     gotoxy(29,21);write('Scorurile sunt');
     gotoxy(29,22);write('salvate si pot fi');
     gotoxy(29,23);write('vizionate apeland');
     gotoxy(29,24);write('meniul SCOREBOARD,');
     gotoxy(29,25);write('unde vor fi afisate');
     gotoxy(29,26);write('doar primele 100 de');
     gotoxy(29,27);write('scoruri, in ordinea');
     gotoxy(29,28);write('descrescatoare a ');
     gotoxy(29,29);write('scorurilor.');
     gotoxy(29,31);write('Fiecarui score, la');
     gotoxy(29,32);write('sfarsitul jocului');
     gotoxy(29,33);write('i se va atribuii');
     gotoxy(29,34);write('un nume de lungime');
     gotoxy(29,35);write('0-3 caractere, urmat');
     gotoxy(29,36);write('de tasta ENTER.');
     gotoxy(29,38);write('Pentru a viziona');
     gotoxy(29,39);write('scorul in timpul ');
     gotoxy(29,40);write('jocului, este ');
     gotoxy(29,41);write('necesara accesarea');
     gotoxy(29,42);write('meniului de pauza');
     gotoxy(29,43);write('care poate fi ');
     gotoxy(29,44);write('accesat folosind');
     gotoxy(29,45);write('tasta ENTER.');
     for i:=1 to 55 do
         Begin
             gotoxy(58,i);
             write(#179);
         end;
     textcolor(white);
     gotoxy(65,2);write('CONTROL:');
     textcolor(lightgray);
     gotoxy(60,6);write('��');
     gotoxy(60,10);write(#17,'��');
     gotoxy(60,13);write(' ',#30);
     gotoxy(60,14);write(' �');
     gotoxy(60,15);write(' �');
     gotoxy(60,17);write(' �');
     gotoxy(60,18);write(' �');
     gotoxy(60,19);write(' ',#31);
     gotoxy(60,22);write('  �');
     gotoxy(60,23);write(#17,'��');
     gotoxy(60,27);write('ESC');
     gotoxy(65,5); write('-Alege directie');
     gotoxy(65,6); write(' dreapta.');
     gotoxy(65,9); write('-Alege directie');
     gotoxy(65,10);write(' stanga.');
     gotoxy(65,14);write('-Alege directie');
     gotoxy(65,15);write(' sus.');
     gotoxy(65,18);write('-Alege directie');
     gotoxy(65,19);write(' jos.');
     gotoxy(65,22);write('-Deschide meniu,');
     gotoxy(65,23);write(' afiseaza scor.');
     gotoxy(65,27);write('-Iesire joc.');
     textcolor(red);
     gotoxy(21,60);write('APASA ENTER PENTRU A TE REINTOARCE IN MENIU');
     readkey;
end;
Procedure wstart(x,y,c:byte);
Begin
     textcolor(c);
     gotoxy(x,y);
     write(' �� ���  �  ��  ���');
     gotoxy(x,y+1);
     write('�    �  � � � �  � ');
     gotoxy(x,y+2);
     write('���  �  ��� ��   � ');
     gotoxy(x,y+3);
     write('  �  �  � � � �  � ');
     gotoxy(x,y+4);
     write('��   �  � � � �  � ');
     gotoxy(x,y+5);
end;
Procedure whelp(x,y,c:byte);
Begin
     textcolor(c);
     gotoxy(x,y);
     write('� � ��� �   �� ');
     gotoxy(x,y+1);
     write('� � �   �   � �');
     gotoxy(x,y+2);
     write('��� ��  �   �� ');
     gotoxy(x,y+3);
     write('� � �   �   �  ');
     gotoxy(x,y+4);
     write('� � ��� ��� �  ');
     gotoxy(x,y+5);
end;
Procedure woptions(x,y,c:byte);
Begin
     textcolor(c);
     gotoxy(x,y);
     write(' �  ��  ��� ���  �  � �  ��');
     gotoxy(x,y+1);
     write('� � � �  �   �  � � ��� �  ');
     gotoxy(x,y+2);
     write('� � ��   �   �  � � � � ���');
     gotoxy(x,y+3);
     write('� � �    �   �  � � � �   �');
     gotoxy(x,y+4);
     write(' �  �    �  ���  �  � � �� ');
     gotoxy(x,y+5);
end;
Procedure wexit(x,y,c:byte);
Begin
     textcolor(c);
     gotoxy(x,y);
     write('��� � � ��� ���');
     gotoxy(x,y+1);
     write('�   � �  �   � ');
     gotoxy(x,y+2);
     write('��   �   �   � ');
     gotoxy(x,y+3);
     write('�   � �  �   � ');
     gotoxy(x,y+4);
     write('��� � � ���  � ');
end;
Procedure wscoreboard(x,y,c:byte);
Begin
     textcolor(c);
     gotoxy(x,y);
     write(' ��  ��  �  ��  ��� ��   �   �  ��  �� ');
     gotoxy(x,y+1);
     write('�   �   � � � � �   � � � � � � � � � �');
     gotoxy(x,y+2);
     write('��� �   � � ��  ��  ��  � � ��� ��  � �');
     gotoxy(x,y+3);
     write('  � �   � � � � �   � � � � � � � � � �');
     gotoxy(x,y+4);
     write('��   ��  �  � � ��� ��   �  � � � � �� ');
     gotoxy(x,y+5);
end;
Procedure Options;
var f:text;
    k:char;
    cursor,i,x,y,p,g,dir,t:shortint;
    opts:array[1..3] of byte;
    path:array[1..20] of record x,y:byte;
                         end;
Begin
     k:=#0;
     clrscr;
     woptions(30,3,white);
     assign(f,'options.in');
     reset(f);
     readln(f,opts[1],opts[2]);
     close(f);
     assign(f,'path.txt');
     reset(f);
     for i:=1 to 20 do
       readln(f,path[i].x,path[i].y);
     close(f);
     x:=15;
     y:=20;
     t:=0;
     textcolor(red);
     gotoxy(x,y);write('GAME SPEED   :',opts[2]:3);
     textcolor(lightgray);
     gotoxy(x,y+7);write('NUMBER GHOSTS:',opts[1]:2);
     gotoxy(x,y+14);write('RESTORE DEFAULT');
     gotoxy(x+3,y+25);write('MENU');
     for i:=1 to 22 do
       Begin
           gotoxy(40,i+16); write('�');
           gotoxy(80,i+16); write('�');
       end;
     for i:=1 to 39 do
       Begin
           gotoxy(i+40,16); write('�');
           gotoxy(i+40,39); write('�');
       end;
     gotoxy(40,16); write('�');
     gotoxy(40,39); write('�');
     gotoxy(80,16); write('�');
     gotoxy(80,39); write('�');
     cursor:=1;
     p:=1;
     g:=18;
     repeat
         if keypressed then
            Begin
               k:=readkey;
               case k of #77:if (cursor = 1) or (cursor = 2) then inc(opts[3-cursor]);
                         #75:if (cursor = 1) or (cursor = 2) then dec(opts[3-cursor]);
                         #72:dec(cursor);
                         #80:inc(cursor);
                         #13:case cursor of 3: Begin opts[1]:=4;opts[2]:=20;end;
                                            4:k:=#27;
                                         end;
                       end;
               if opts[2]<10 then opts[2]:=10;
               if opts[2]>100 then opts[2]:=100;
               if opts[1]<1 then opts[1]:=1;
               if opts[1]>7 then opts[1]:=7;
               if cursor<1 then cursor:=1;
               if cursor>4 then cursor:=4;
               if cursor=1 then textcolor(red) else textcolor(lightgray);
               gotoxy(x,y);write('GAME SPEED   :',opts[2]:3);
               if cursor=2 then textcolor(red) else textcolor(lightgray);
               gotoxy(x,y+7);write('NUMBER GHOSTS:',opts[1]:2);
               if cursor=3 then textcolor(red) else textcolor(lightgray);
               gotoxy(x,y+14);write('RESTORE DEFAULT');
               if cursor=4 then textcolor(red) else textcolor(lightgray);
               gotoxy(x+3,y+25);write('MENU');
            end;
         if (k<>#27) and (t=10) then
          Begin
           i:=g-opts[1];
           if i<1 then i:=20+i;
           ghost(path[i].x,path[i].y,black,true);
           i:=g-opts[1]-1;
           if i<1 then i:=20+i;
           ghost(path[i].x,path[i].y,black,true);
           ghost(path[g].x,path[g].y,red,true);
           if p=1 then
           pacman(path[20].x,path[20].y,dir,black)
           else
           pacman(path[p-1].x,path[p-1].y,dir,black);
           case p of 1:dir:=3;
                     8:dir:=4;
                     11:dir:=2;
                     18:dir:=1;
                    end;
           pacman(path[p].x,path[p].y,dir,yellow);
           inc(p);if p>20 then p:=1;
           inc(g);if g>20 then g:=1;
           t:=0;
          end;
         inc(t);
         delay(opts[2]);
     until k=#27;
     assign(f,'options.in');
     rewrite(f);
     write(f,opts[1],' ',opts[2]);
     close(f);
end;
Procedure smenu(xm,ym:byte);
Begin
        textbackground(black);
        clrscr;
        textcolor(yellow);
        writeln;
        writeln;
        writeln;
        writeln;
        writeln;
        writeln('                                        �����                   ');
        writeln('                                      ���������           ');
        writeln('                                     �����  ����           ');
        writeln('                                     ����� ����                   ');
        writeln('                                    ����������              ');
        writeln('                                    ��������                    ');
        writeln('                                    �������                ');
        writeln('                                    ��������                   ');
        writeln('                                    ����������                  ');
        writeln('                                     ����������             ');
        writeln('                                     �����������                   ');
        writeln('                                      ���������                     ');
        writeln('                                        �����                ');
        wstart(xm+10,ym,white);
        whelp(xm+12,ym+7,white);
        wscoreboard(xm,ym+14,white);
        woptions(xm+6,ym+21,white);
        wexit(xm+12,ym+28,white);
end;
Procedure cub(x,y,c:byte);
var j:byte;
Begin
    textbackground(c);
    for j:=1 to 7 do
      Begin
         gotoxy(x,y+j-1);
         write('                 ');
      end;
end;
procedure SCOREBOARD;
var f:text;
    n,i,j:byte;
    v:arpers;
    x:per;
    sem:boolean;
Begin
     assign(f,'scores.in');
     reset(f);
     n:=0;
     while not eof(f) do
         Begin
             inc(n);
             readln(f,v[n].n,v[n].s);
         end;
     clrscr;
     close(f);
     for i:=1 to n-1 do
        for j:=i to n do
            if v[i].s<v[j].s then
               Begin
                  x:=v[i];
                  v[i]:=v[j];
                  v[j]:=x;
               end;
     cub(13,2,cyan);
     textcolor(black);
     gotoxy(20,4);write(v[1].n);
     cub(35,2,white);
     gotoxy(42,4);write(v[2].n);
     cub(57,2,green);
     gotoxy(64,4);write(v[3].n);
     textbackground(black);
     textcolor(white);
     gotoxy(19,9);write(v[1].s);
     gotoxy(41,9);write(v[2].s);
     gotoxy(63,9);write(v[3].s);
     if n>100 then n:=100;
     for i:=3 to n do
        Begin
           gotoxy(((i-3) div 20)*15+8,((i-3) mod 20)*2+15);
           write(v[i].n,'-->',v[i].s);
        end;
     readkey;
     assign(f,'scores.in');
     rewrite(f);
     for i:=1 to n do
        writeln(f,upcase(v[i].n[1]),upcase(v[i].n[2]),upcase(v[i].n[3]),v[i].s);
     close(f);
end;
Begin
     randomize;
     i:=1;
     xm:=24;
     ym:=24;
     k:=#27;
     clrscr;
     smenu(xm,ym);
     cursoroff;
     textbackground(black);
     repeat
           case i of 1:wstart(xm+10,ym,red);
                     2:whelp(xm+12,ym+7,red);
                     3:wscoreboard(xm,ym+14,red);
                     4:woptions(xm+6,ym+21,red);
                     5:wexit(xm+12,ym+28,red);
                  end;
           k:=readkey;
           textcolor(lightgray);
           case i of 1:wstart(xm+10,ym,white);
                     2:whelp(xm+12,ym+7,white);
                     3:wscoreboard(xm,ym+14,white);
                     4:woptions(xm+6,ym+21,white);
                     5:wexit(xm+12,ym+28,white);
                  end;
           case k of #72:dec(i);
                     #80:inc(i);
                     #13:case i of 1:Begin
                                        GAME;
                                        smenu(xm,ym);
                                     end;
                                   2:Begin
                                        HELP;
                                        smenu(xm,ym);
                                     end;
                                   3:Begin
                                        scoreboard;
                                        smenu(xm,ym);
                                     end;
                                   4:Begin
                                        OPTIONS;
                                        smenu(xm,ym);
                                     end;
                                   5:k:=#27;
                                end;
                  end;
           if i<1 then i:=1;
           if i>5 then i:=5;
     until k=#27;
end.
